<?php

namespace Tests;

use dubinsky\uritils\Uritils;

/**
 * php vendor/bin/phpunit tests/UritilsTest.php
 * 
 * @author dubinsky
 */
class ClientServerTest extends \PHPUnit_Framework_TestCase {

    public static function setStrict($errno, $errstr, $errfile, $errline) {
	throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
    }

    /**
     * run:
     * php ./vendor/bin/phpunit ./tests/UritilsTest.php
     */
    public function testClientServer() {

        $uritil = new Uritils();
        
	//domains trigger warnings
        $this->assertEquals('blogspot.com',
            $uritil->getDomain('ty--2.blogspot.com')
        );
        
        $this->assertEquals('com',
            $uritil->getDomainZone('ty--2.blogspot.com')
        );

	$this->assertEquals('ty--2.blogspot.com',
            $uritil->getHost('ty--2.blogspot.com/asd/fg/')
        );
	
	$this->assertEquals('ty--2.blogspot.com',
            $uritil->getHost('ty--2.blogspot.com')
        );

        $this->assertEquals('ty--2.blogspot.com',
	       $uritil->getAsciiUrl("ty--2.blogspot.com")
        );
        set_error_handler([__CLASS__, 'setStrict'] );
        
        //failed funcs
        $this->assertNull($uritil->getDomain('notadomain'));

        //simple tests
        
        $this->assertEquals('github.com',
            $uritil->getDomain("https://github.com/master/composer.json")
        );
        $this->assertEquals('github.co.uk',
            $uritil->getDomain("https://github.co.uk/master/composer.json")
        );
        $this->assertEquals('github.co.uk',
            $uritil->getDomain("github.co.uk/master/composer.json")
        );
        $this->assertEquals('ukm.com',
            $uritil->getDomain("https://github.co.ukm.com/master/composer.json")
        );
        $this->assertEquals("xn--80adkuocecm.xn--p1ai",
            $uritil->getDomain('https://www.поваренок.рф/imghp?hl=en&tab=wi&authuser=0')
        );
        $this->assertEquals("xn--80adkuocecm.xn--p1ai",
            $uritil->getDomain('www.поваренок.рф/imghp?hl=en&tab=wi&authuser=0')
        );
        $this->assertEquals("xn--80adkuocecm.xn--p1ai",
            $uritil->getDomain('поваренок.рф/imghp?hl=en&tab=wi&authuser=0')
        );
        //PUNICODE funcs
        $this->assertEquals('https://выа.co.ukm/',
            $uritil->getPunycodeUrl("https://xn--80ad9e.co.ukm/")
        );
        $this->assertEquals('https://www.поваренок.рф/imghp?hl=en&tab=wi&authuser=0',
            $uritil->getPunycodeUrl("https://www.xn--80adkuocecm.xn--p1ai/imghp?hl=en&tab=wi&authuser=0")
        );
        $this->assertEquals('www.поваренок.рф/imghp?hl=en&tab=wi&authuser=0',
            $uritil->getPunycodeUrl("www.xn--80adkuocecm.xn--p1ai/imghp?hl=en&tab=wi&authuser=0")
        );
        //punicode path
        $this->assertEquals('http://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0',
            $uritil->getAsciiUrl("http://ru.wikipedia.org/wiki/Заглавная_страница")
        );
        /*
        $this->assertEquals('ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0',
            $uritil->getAsciiUrl("ru.wikipedia.org/wiki/Заглавная_страница")
        );
        */

        //ASCII URL
        $this->assertEquals("https://xn--80ad9e.co.ukm/",
            $uritil->getAsciiUrl("https://выа.co.ukm/")
        );
        $this->assertEquals("https://xn--80ad9e.co.ukm",
            $uritil->getAsciiUrl("https://выа.co.ukm")
        );
        $this->assertEquals("https://www.xn--80adkuocecm.xn--p1ai/imghp?hl=en&tab=wi&authuser=0",
            $uritil->getAsciiUrl('https://www.поваренок.рф/imghp?hl=en&tab=wi&authuser=0')
        );
        $this->assertEquals("www.xn--80adkuocecm.xn--p1ai/imghp?hl=en&tab=wi&authuser=0",
            $uritil->getAsciiUrl('www.поваренок.рф/imghp?hl=en&tab=wi&authuser=0')
        );

        //HOST function
        $this->assertEquals("xn--80ad9e.co.ukm",
            $uritil->getHost("https://выа.co.ukm/asasss")
        );

        $this->assertEquals("xn--80ad9e.co.ukm",
            $uritil->getHost("xn--80ad9e.co.ukm")
        );

        //domain zone
        $this->assertEquals("ukm",
            $uritil->getDomainZone("https://выа.co.ukm/asasss")
        );

        $this->assertEquals('xn--p1ai',
            $uritil->getDomainZone('www.поваренок.рф/imghp?hl=en&tab=wi&authuser=0')
        );
    }
}