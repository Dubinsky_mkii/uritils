<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.20.1
 * Time: 14:03
 */

namespace dubinsky\uritils;

class Uritils {

    public function stripUrl($url) {
        if(stripos($url, 'http://') === 0) $url = substr($url, 7);
        if(stripos($url, 'https://') === 0) $url = substr($url, 8);
        if(stripos($url, 'www.') === 0) $url = substr($url, 4);
        return rtrim($url,  '/');
    }

    public function canonizeUrl($url) {
        if  (stripos($url, 'http://') === 0 ||
            stripos($url, 'https://') === 0) {
            return $url;
        } else {
            $url = "http://".$url;
            return $url;
        }
    }

    public function isValidHTTPDomain($url) {
        if (isset($url) && strlen($url) > 0) {
            $url = $this->canonizeUrl($url);
            $encodedurl = $this->getAsciiUrl($url);
            $url = filter_var($encodedurl, FILTER_SANITIZE_URL);
            if (filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED) === false ||
                strpos($url, '.') === false) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @param $url
     * @return string ASCII hostname if this url without trailing slash
     */
    public function getHost($url) {
        $url = $this->getAsciiUrl($url);
        $info = $this->safeParseUrl($url);
        if ($info) {
            $hostOriginal = array_key_exists('host', $info) ? $info["host"] : '';
            $host = idn_to_ascii($hostOriginal, null, INTL_IDNA_VARIANT_UTS46);
	    if (strlen($host) == 0) {
                //some domains like ty--.blogspot.com can not be translated by ucs standart
                $host = idn_to_ascii($host, null, INTL_IDNA_VARIANT_2003);
            }
	    if (strlen($host) == 0) {//failsafe conversion for ty--2.blogspot.com type of domains
		$host = $hostOriginal;
	    }

            return $this->stripUrl($host);
        } else {
            //TODO: emit warning?
            return $url;
        }
    }

    /**
     * @param $url
     * @return string
     */
    public function getDomainZone($url) {
        $host = $this->getHost($url);
        $dom_rev = strrev($host);
        $dot_pos = strpos($dom_rev, '.');
        return strrev(substr($dom_rev, 0, $dot_pos));
    }


    /**
     * fallback method for retreiving domain information
     * @param $url
     * @return string
     */
    private function getDomainSimple($url) {
        $url = $this->canonizeUrl($url);
        $info = parse_url($url);
        $host = $info['host'];
        $host_names = explode(".", $host);
        if (sizeof($host_names) <= 1) {//we cant find domain
            return null;
        }
        $bottom_host_name = $host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1];
        return $bottom_host_name;
    }

    /**
     *
     * @param $url
     * @return bool
     */
    private function getDomain2($url) {
        $url = $this->canonizeUrl($url);
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : '';
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }

    /**
     * @param $url string url string
     * @return string ASCII domain only
     */
    public function getDomain($url) {
        $url = $this->getAsciiUrl($url);
        $domainRes = $this->getDomain2($url);
        if (strlen($domainRes) == 0) {
            return $this->getDomainSimple($url);
        }
        return $domainRes;
    }

    /**
     * @param $url string url address possibly containing punicode converted to acsii
     * @return string UTF url string
     */
    public function getPunycodeUrl($url) {
        $info = $this->safeParseUrl($url);
        if ($info) {
            $scheme = array_key_exists('scheme', $info) ? $info["scheme"] : '';
            $host = array_key_exists('host', $info) ? $info["host"] : '';
            $path = array_key_exists('path', $info) ? $info["path"] : '';
            $query = array_key_exists('query', $info) ? $info["query"] : '';
            $hostConverted = idn_to_utf8($host, IDNA_NONTRANSITIONAL_TO_ASCII, INTL_IDNA_VARIANT_UTS46);
            if (strlen($hostConverted) == 0) {
                //some domains like ty--.blogspot.com can not be translated by ucs standart
                $hostConverted = idn_to_utf8($host, null, INTL_IDNA_VARIANT_2003);
            }
            $result = $hostConverted.$path;
            if (strlen($scheme) > 0) {
                $result = $scheme."://".$result;
            }
            if (strlen($query) > 0) {
                $result .= "?".$query;
            }
            return $result;
        } else {
            //TODO: emit warning?
            return $url;
        }
    }

    /**
     * @param $url string url address possibly punicode
     * @return string ASCII url string
     */
    public function getAsciiUrl($url) {
        $info = $this->safeParseUrl($url);
        if ($info) {
            $scheme = array_key_exists('scheme', $info) ? $info["scheme"] : '';
            $host = array_key_exists('host', $info) ? $info["host"] : '';
            $path = array_key_exists('path', $info) ? $info["path"] : '';
            $query = array_key_exists('query', $info) ? $info["query"] : '';
            $hostConverted = idn_to_ascii($host, IDNA_NONTRANSITIONAL_TO_ASCII, INTL_IDNA_VARIANT_UTS46);
            if (strlen($hostConverted) == 0) {
                //some domains like ty--.blogspot.com can not be translated by ucs standart
                $hostConverted = idn_to_ascii($host, null, INTL_IDNA_VARIANT_2003);
            }
            //glue path back converting it to
            $path = explode("/", $path);
            array_walk( $path, function(&$val) { $val = urlencode($val); } );
            $path = implode("/", $path);
            $result = $hostConverted.$path;
            if (strlen($scheme) > 0) {
                $result = $scheme."://".$result;
            }
            if (strlen($query) > 0) {
                $result .= "?".$query;
            }
            return $result;
        } else {
            //TODO: emit warning?
            return $url;
        }
    }

    /**
     * php parse url with fallback in case scheme not found for idn funcs to work and host is in path not host
     * @param $url
     * @return mixed
     */
    private function safeParseUrl($url) {
        $info = parse_url($url);
        if ($info) {
            if (!array_key_exists('host', $info)) {
                $fallbackUrl = "http://" . $url;
                $fallbackInfo = parse_url($fallbackUrl);
                if ($fallbackInfo) {
                    if (array_key_exists('host', $fallbackInfo)) {
                        $info = $fallbackInfo;
                        unset($info['scheme']);//ignore fallback scheme
                    }
                }
            }
        }
        return $info;
    }

}